// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const MAX_LIMIT = 100;
const db = cloud.database();

// 云函数入口函数
exports.main = async (event, context) => {
  // console.log(event)
  switch (event.action) {
    case 'sendTemplateMessage': {
      return sendTemplateMessage(event)
    }
    case 'getWXACode': {
      return getWXACode(event)
    }
    case 'getOpenData': {
      return getOpenData(event)
    }
    case 'msgSecCheck': {
      return msgSecCheck(event)
    }
    case 'imgSecCheck': {
      return imgSecCheck(event)
    }
    case 'submitPages': {
      return submitPages(event)
    }
    default: {
      return
    }
  }
}

//获取推送消息
async function sendTemplateMessage(event) {
  const { OPENID, APPID, UNIONID } = cloud.getWXContext() // 这里获取到的 openId 和 appId 是可信的

  // 接下来将新增模板、发送模板消息、然后删除模板
  // 注意：新增模板然后再删除并不是建议的做法，此处只是为了演示，模板 ID 应在添加后保存起来后续使用
  // const addResult = await cloud.openapi.templateMessage.addTemplate({
  //   id: 'AT0008',
  //   keywordIdList: [3, 4]
  // })

  // const templateId = addResult.templateId

  // await cloud.openapi.templateMessage.deleteTemplate({
  //   templateId,
  // })
  // 先取出集合记录总数
  const countResult = await db.collection('collectformid').count();
  const total = countResult.total;

  // 计算需分几次取
  const batchTimes = Math.ceil(total / 100)
  // 承载所有读操作的 promise 的数组
  const tasks = []
  for (let i = 0; i < batchTimes; i++) {
    const _ = db.command;
    const time = new Date().getTime() - 7 * 1000 * 24 * 60 * 60;
    const promise = db.collection('collectformid').skip(i * MAX_LIMIT).limit(MAX_LIMIT).where({
      timestamp: _.gte(time)
    }).orderBy('timestamp', 'asc').get()
    tasks.push(promise);
  }
  // 等待所有
  const pushAttr = (await Promise.all(tasks)).reduce((acc, cur) => {
    return {
      data: acc.data.concat(cur.data),
      errMsg: acc.errMsg,
    }
  })
  let hash = [];
  for (let i of pushAttr.data) {
    if (hash.indexOf(i._openid) == -1) {
      hash.push(i._openid);
      db.collection('collectformid').doc(i._id).remove();
      const sendResult = await cloud.openapi.templateMessage.send({
        touser: i._openid,
        templateId: event.templateId,
        formId: i.formId,
        page: event.page,
        emphasisKeyword: event.keyWord == 1 ? 'keyword1.DATA' : event.keyWord == 2 ? 'keyword2.DATA' : '',
        data: {
          keyword1: {
            value: event.content,
          },
          keyword2: {
            value: getDateTime(new Date().getTime() / 1000)
          }
        }
      })
    }
    // return sendResult
  }
  return hash.length
}

//获取小程序码
async function getWXACode(event) {
  console.log(event.url)
  // 此处将获取永久有效的小程序码，并将其保存在云文件存储中，最后返回云文件 ID 给前端使用

  const wxacodeResult = await cloud.openapi.wxacode.get({
    path: event.url || 'pages/index/index',
  })

  const fileExtensionMatches = wxacodeResult.contentType.match(/\/([^\/]+)/)
  const fileExtension = (fileExtensionMatches && fileExtensionMatches[1]) || 'jpg'

  const uploadResult = await cloud.uploadFile({
    // 云文件路径，此处为演示采用一个固定名称
    cloudPath: `wxCode/wxCode${Math.random() * 9999999}.${fileExtension}`,
    // 要上传的文件内容可直接传入图片 Buffer
    fileContent: wxacodeResult.buffer,
  })

  if (!uploadResult.fileID) {
    throw new Error(`上传失败，文件为空，存储服务器状态代码为空 ${uploadResult.statusCode}`)
  }

  return uploadResult.fileID
}

// 获取openid
async function getOpenData(event) {
  // 需 wx-server-sdk >= 0.5.0
  return cloud.getOpenData({
    list: event.openData.list,
  })
}

// 检测文本是否合规
async function msgSecCheck(event) {
  // 需 wx-server-sdk >= 0.5.0
  return cloud.openapi.security.msgSecCheck({
    content: event.content,
  })
}

// 检测图片是否合规
async function imgSecCheck(event) {
  return cloud.openapi.security.imgSecCheck({
    media: {
      contentType: event.contentType,
      value: Buffer.from(event.value)
    }
  })
}

// 收录页面
async function submitPages(event) {
  return cloud.openapi.search.submitPages({
    pages: [{
      path: event.path,
      query: event.query
    }]
  })
}




//获取日期
function getDateTime(sj) {
  var now = new Date(sj * 1000);
  var year = now.getFullYear();
  var month = now.getMonth() + 1;
  var date = now.getDate();
  var hour = now.getHours();
  var minute = now.getMinutes();
  // var second = now.getSeconds();
  return year + "年" + month + "月" + date + "日";
}