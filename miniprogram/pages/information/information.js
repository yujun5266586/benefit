const db = wx.cloud.database();
import util from '../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:'',
    address:'',
    contacts:'',
    phone:'',
    issue:'',
    patientAttr: ['确诊', '疑似', '密切接触者', '无法排出的发热者', '其他'],
    patientType: -1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  /**
   * 更改水印位置
   */
  bindPickerChange(e) {
    const that = this;
    that.setData({
      patientType: e.detail.value
    })
  },
  bindInput:async function (e) {
    const that = this;
    if(e.detail.value.length!=0){
      const checkFont = await util.checkFont(e.detail.value);
      console.log("321:",checkFont)
      switch(e.target.dataset.type) {
        case 'name':
          that.setData({
            name: checkFont==0?e.detail.value:''
          })
          break;
        case 'address':
          that.setData({
            address: checkFont==0?e.detail.value:''
          })
          break;
        case 'contacts':
          that.setData({
            contacts: checkFont==0?e.detail.value:''
          })
          break;
        case 'phone':
          that.setData({
            phone:checkFont==0?e.detail.value:''
          })
          break;
        default:
          that.setData({
            issue: checkFont==0?e.detail.value:''
          })
      } 
    }
    
  },
  /**
   * 点击提交信息
   */
  onSubmitClicked: function () {
    const that = this;
    if(!that.data.name){
      wx.showToast({
        icon:'none',
        title: '请输入姓名~',
      })
      return false;
    }
    if(that.data.patientType==-1){
      wx.showToast({
        icon:'none',
        title: '请选择问题类别~',
      })
      return false;
    }
    if(!that.data.address){
      wx.showToast({
        icon:'none',
        title: '请输入地址~',
      })
      return false;
    }
    if(!that.data.contacts){
      wx.showToast({
        icon:'none',
        title: '请输入联系人~',
      })
      return false;
    }
    if(!that.data.phone){
      wx.showToast({
        icon:'none',
        title: '请输入联系电话~',
      })
      return false;
    }
    if(!that.data.issue){
      wx.showToast({
        icon:'none',
        title: '请输入您的身体状况~',
      })
      return false;
    }
    db.collection('information').add({
      data: {
        name:that.data.name,
        address:that.data.address,
        contacts:that.data.contacts,
        phone:that.data.phone,
        issue:that.data.issue,
        patientType: that.data.patientAttr[that.data.patientType],
        date: new Date(),
      },
      success: function(res) {
        wx.showToast({
          title: '信息提交成功~',
        })
        console.log(res)
      }
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})