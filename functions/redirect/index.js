// 云函数入口文件
const cloud = require('wx-server-sdk')
var https = require('https'); //http模块
cloud.init()
// 云函数入口函数
exports.main = async (event, context) => {
  var option = {
    hostname: 'u1qa5f.smartapps.cn',
    path: `/webmapp/api/v1/proxy?url=https%3A%2F%2Fcityservice-hb.cdn.bcebos.com%2Famis%2Fpneumonia%2Ftravel_data.json%3Fsign%3DidKkFefMLgPI2%252BnMuaO3RlsXmgDw58EcLcqx1JX4i8mQf1%252FBGiSCwaeVzDuJ%252BUoNWZWMqHCKyC%252F7RTKqho%252FZtbdozOPuBYmu9g2PDYcbihlGkjsYHe%252Fknpy2Q5EJ6Wzpn0STcN1gE6Hum6xZP00zCkbeSeeXhySu2wRE0A7dfyU%253D`,
    headers: {
      'Content-Type':'application/json',
      'Accept': '*/*',
      'Accept-Encoding': 'utf-8',  //这里设置返回的编码方式 设置其他的会是乱码
      'Accept-Language': 'zh-CN,zh;q=0.8',
      'Connection': 'keep-alive',
      'Referer': 'https://u1qa5f.smartapps.cn/showmaster/?appKey=BqMNg4PRZq9U4GlZLMdOkEwBbbKLMGhu'
    }
  };
  return new Promise((resolve, reject) => {
    try {
      https.get(option, (res) => {
        var html = '';
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
          html += chunk;
        })
        res.on('end', function () {
          return resolve(html)
        })
      })
    } catch (e) {
      return reject(e)
    }
  })
}