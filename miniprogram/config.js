/**
 * 小程序配置文件
 */
const version = '4.2.3';
const stage = 'develop-i3rm4'; //后台环境 develop-i3rm4 master-19at3

const config = {
    // 下面的地址配合云端 Server 工作
    version,
    stage,

    /*
    * 获取人像信息
    * @param {*} api_key
    */
    getFaceInfoUrl: `https://312/facepp/v3/detect`,

};
module.exports = config